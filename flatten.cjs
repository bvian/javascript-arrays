function flatten (elements, depth) {
    if (typeof elements !== 'object')  {
        return [];
    }

    depth = (depth === undefined || typeof depth !== 'number')? 1 : depth;
    
    let flatArray = [];

    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index]) && depth !== 0) {
            flatArray = flatArray.concat(flatten(elements[index], depth - 1));
        }
        else {
            elements[index] !== undefined? flatArray.push(elements[index]): null;
        }
    }

    return flatArray;

}

module.exports = flatten;