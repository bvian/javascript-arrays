function reduce(elements, cb, startingValue) {
    if (typeof elements !== 'object' || typeof cb !== 'function') {
        return [];
    }

    let index = 0, sum;

    if (startingValue === undefined) {
        startingValue = elements[0];
        index = 1;
    }
    
    for (; index < elements.length; index++) {
        sum = cb(startingValue, elements[index], index, elements);
        startingValue = sum;
    }

    return sum;
}

module.exports = reduce;
