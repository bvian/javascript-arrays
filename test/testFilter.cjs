const { items } = require('./items.cjs');
const filter = require('../filter.cjs');

const cb = (value, index, items) => {
    return items[index] > 3;
}

const result = filter(items, cb);

console.log(result);