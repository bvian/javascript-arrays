const flatten = require('../flatten.cjs');

const nestedArray = [1, [2], [[3]], [[[4]]]];
const depth = 2;

const result = flatten(nestedArray, depth);

console.log(result);