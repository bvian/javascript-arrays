function map(elements, cb) {
    if (typeof elements != 'object' || typeof cb !== 'function') {
        return [];
    }

    const newArray = [];

    for (let index = 0; index < elements.length; index++) {
        newArray.push(cb(elements[index], index, elements));
    }

    return newArray;
}

module.exports = map;